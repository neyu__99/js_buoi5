// ex1
document.getElementById('Ketqua').onclick=function(){
    // input khai báo điểm chuẩn, điểm các môn, điểm khu vực đối tượng 
    var DiemChuan = Number(DomId('DiemChuan'))
    var Diem1 = Number(DomId('Diem1'))
    var Diem2 = Number(DomId('Diem2'))
    var Diem3 = Number(DomId('Diem3'))

    var KhuVuc=document.getElementById('KhuVuc')
    var KhuVucChon = KhuVuc.options[KhuVuc.selectedIndex].value;
    var DoiTuong=document.getElementById('DoiTuong')
    var DoiTuongChon = DoiTuong.options[DoiTuong.selectedIndex].value;

    var DiemUuTien=TinhDiemUuTien(KhuVucChon)+TinhDiemUuTien(DoiTuongChon)
    // output tổng điểm
    var TongDiem=Diem1+Diem2+Diem3+DiemUuTien
    // progress: Tính điểm ưu tiên dựa theo khu vực, đối tượng đã chọn. Tính tổng điểm. Xét trường hợp điểm các môn khác 0 so sánh với điểm chuẩn
    if ((Diem1!=0)&&(Diem2!=0)&&(Diem3!=0)){
        if (TongDiem>=DiemChuan){document.getElementById('TongDiem').innerHTML=`Bạn đã đậu. Tổng điểm: ${TongDiem}`}
        else {document.getElementById('TongDiem').innerHTML=`Bạn đã rớt. Tổng điểm: ${TongDiem}`}
    }
    else {document.getElementById('TongDiem').innerHTML=`Bạn đã rớt do có điểm bằng 0`}  
}
function TinhDiemUuTien(x){
        switch(x){
            case 'A': return 2;
            case 'B': return 1;
            case 'C': return 0.5;
            case '0': return 0;
            case '1': return 2.5;
            case '2': return 1.5;
            case '3': return 1;
        }
}

// ex2
document.getElementById('TinhTien').onclick=function(){
    // input: họ tên, số kw
    var HoTen = DomId('HoTen')
    var SoKw = Number(DomId('SoKw'))
    const To50=500
    const To100=650
    const To200=850
    const To350=1100
    const Over350=1300

    // output: tổng tiền điện
    var TienDien=0
    // progress: xét các trường hợp theo quy tắc
    if (SoKw<=50){TienDien=SoKw*To50}
    else if (SoKw<=100){TienDien=(50*To50)+((SoKw-50)*To100)}
    else if (SoKw<=200){TienDien=(50*To50)+(50*To100)+((SoKw-100)*To200)}
    else if (SoKw<=350){TienDien=(50*To50)+(50*To100)+(100*To200)+((SoKw-200)*To350)}
    else if (SoKw>350){TienDien=(50*To50)+(50*To100)+(100*To200)+(150*To350)+((SoKw-350)*Over350)}
    document.getElementById('TongTien').innerHTML= `Họ tên: ${HoTen} ; Tiền điện: ${TienDien.toLocaleString()} VND`
}

function DomId(Id) {
    return document.getElementById(Id).value
}
